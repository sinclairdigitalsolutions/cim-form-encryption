# Compulse Form Encryption Plugin
## Description
For more information about this plugin and how to use it, see this doc: https://docs.google.com/document/d/1FF62UirJyIqMEvOSGKBvBZbxN7h_l5dWhTI2inB36Zo/edit?usp=sharing

## Initial Setup

#### 1. Download WP Pusher Plugin if you haven't so already (Optional)
_If you already have wppusher.zip downloaded, you may skip this step._

[WP Pusher](https://wppusher.com/download "Click to download WP Pusher") [Click Link To Download]

#### 2. Install Downloaded Plugin
* Go to Plugins on the left menu on the Admin Dashboard inside the WordPress backend.
* Hit __Add New__
* Hit __Upload Plugin__
* Choose wppusher.zip and hit __Install Now__
* Once installed, press __Activate__

#### 3. Configuring WP Pusher
__IMPORTANT__ - If you are user on BitBucket, please ensure you __LOG OFF__ before proceeding with this step. You will be using a read only credential to generate the necessary token.

##### If you are NOT logged into the Bitbucket account with solutions@compulse.com
* Go to the Bitbucket Tab  
* Hit __Obtain a Bitbucket token__
* You should see a field to login to BitBucket. Use the credentials below.
    * Username: solutions@compulse.com
    * Password: (Ask a dev for the password)
* Copy the generated token into the _Bitbucket token_ field.
* Hit __Save Bitbucket Token__
* At this point you will not need to log into the Bitbucket account again, when you get the token next time it will not ask for authentication.

##### If you ARE logged into the Bitbucket account with solutions@compulse.com
* Hit __Obtain a Bitbucket token__
* When you are logged in, you will skip the authentication process.
* Copy the generated token into the _Bitbucket token_ field.
* Hit __Save Bitbucket Token__

#### 4. Installing Updated Compulse Form Encryption Plugin
* On the left sidebar in the WP Pusher menu, press __Install Plugin__
* Choose __Bitbucket__ as the __Repository Host__
* The plugin repository is  
~~~
sinclairdigitalsolutions/cim-form-encryption
~~~
* Leave __Repository branch__ and __Repository subdirectory__ empty
* Make sure __Push-to-Deploy__ option is checked
* Hit __Install Plugin__
* Once Installed there will be a message at the top of the screen. Click the __activate__ link.
> Plugin was successfully installed. Go ahead and activate it.

#### 5. Complete
