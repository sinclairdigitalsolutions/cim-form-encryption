<?php

class CIMFormEncryptionPlugin {
    private static $instance = null;

    public static function get_instance() {
        if ( empty(self::$instance) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private $active_plugins;

    private $admin_notices;

    public function __construct() {
        $this->active_plugins = get_option('active_plugins', array());
        $this->admin_notices = array();

        add_action('admin_notices', array($this, 'admin_notices'));
        add_action('admin_menu', array($this, 'add_options_page'));
        add_action('admin_init', array($this, 'admin_init'));

        // print $this->encrypt_message( 'test' );
        // exit;

        // Check for Contact Form 7 and Contact Form DB.
        if ( !get_option('cim_form_encryption_disable_cf7', false) && in_array('contact-form-7/wp-contact-form-7.php', $this->active_plugins) ) {
            if ( in_array('contact-form-7-to-database-extension/contact-form-7-db.php', $this->active_plugins) ) {
                CIMContactForm7Encryption::get_instance();
            } else {
                $this->add_admin_notice('ERROR: To enable the encrypted field for Contact Form 7, the Compulse Encrypted Fields plugin also requires Contact Form DB.', 'error');
            }
        }

        // Check for WP Job Manager.
        if ( !get_option('cim_form_encryption_disable_wp_job_manager', false) &&
            in_array('wp-job-manager/wp-job-manager.php', $this->active_plugins) &&
            in_array('wp-job-manager-applications/wp-job-manager-applications.php', $this->active_plugins) ) {
                CIMWPJobManagerEncryption::get_instance();
        }
    }

    private function get_public_key() {
        $public_key = false;
        $ascii_public_key = get_option('cim_form_encryption_public_key', '');

        if ( !empty($ascii_public_key) ) {
            $matches = array();
            $type = 'MESSAGE';

            if ( preg_match( '/-----BEGIN ([A-Za-z ]+)-----/', $ascii_public_key, $matches ) ) {
                $type = $matches[1];
            }

            $unarmored_public_key = OpenPGP::unarmor( $ascii_public_key, $type );

            $result = OpenPGP_Message::parse( $unarmored_public_key );
            if ( !empty($result) ) {
                $public_key = $result;
            }
        }

        return $public_key;
    }

    private function get_aes_encryption_key() {
        return defined('CIM_FORM_ENCRYPTION_KEY') ? CIM_FORM_ENCRYPTION_KEY : '21xOkup%&NXA37N@T4e$AKOe2Zkff4c#';
    }

    private function get_aes_encryption_salt() {
        return defined('CIM_FORM_ENCRYPTION_SALT') ? CIM_FORM_ENCRYPTION_SALT : 'MVb@u1kzAj5S#u0dVz5Kw&rgBEZ7TOq9';
    }

    public function get_encryption_method() {
        return !get_option('cim_form_encryption_use_aes_encryption', false) ? 'PGP' : 'AES';
    }

    public function encrypt_message($message, $method = false) {
        if ( !$method ) {
            $method = $this->get_encryption_method();
        }

        $encrypted_message = false;

        if ( $method == 'PGP' ) {
            $public_key = $this->get_public_key();

            if ( $public_key ) {
                $literal = new OpenPGP_LiteralDataPacket($message, array(
                    'format' => 'u',
                    'filename' => 'encrypted.gpg'
                ));

                $encrypted_object = OpenPGP_Crypt_Symmetric::encrypt( $public_key, new OpenPGP_Message(array($literal)) );

                //$encrypted_message = wordwrap( OpenPGP::enarmor($encrypted_object->to_bytes(), 'PGP MESSAGE', array()), 64, "\n", true );
                $encrypted_message = OpenPGP::enarmor($encrypted_object->to_bytes(), 'PGP MESSAGE', array());
            }
        } elseif ( $method == 'AES' ) {
            $data = $message;
            $key = $this->get_aes_encryption_key();
            $salt = $this->get_aes_encryption_salt();
            $cipher = 'AES-128-CBC';
         	$ivLength = openssl_cipher_iv_length($cipher);
         	$iv = openssl_random_pseudo_bytes($ivLength);
            $data .= $salt;
         	$cipherTextRaw = openssl_encrypt($data, $cipher, $key, OPENSSL_RAW_DATA, $iv);
         	$hmac = hash_hmac('sha256', $cipherTextRaw, $key, true);
         	$encrypted_message = base64_encode($iv . $hmac . $cipherTextRaw);
        }

        return $encrypted_message;
    }

    public function decrypt_aes_message($data) {
        $decrypted_message = false;

        $key = $this->get_aes_encryption_key();
        $salt = $this->get_aes_encryption_salt();
        $cipher = 'AES-128-CBC';
     	$ivLength = openssl_cipher_iv_length($cipher);
     	$content = base64_decode($data);
     	$iv = substr($content, 0, $ivLength);
     	$hmac = substr($content, $ivLength, $sha2len = 32);
     	$cipherTextRaw = substr($content, $ivLength + $sha2len);
     	$calcMac = hash_hmac('sha256', $cipherTextRaw, $key, true);
        
     	if (hash_equals($hmac, $calcMac)) {
     		$result = openssl_decrypt($cipherTextRaw, $cipher, $key, OPENSSL_RAW_DATA, $iv);
            $result = substr($result, 0, strlen($result) - strlen($salt));
            $decrypted_message = $result;
     	}

        return $decrypted_message;
    }

    public function admin_init() {
        // Check CF7 version.
        if ( in_array('contact-form-7/wp-contact-form-7.php', $this->active_plugins) ) {
            $plugin_file = WP_PLUGIN_DIR . '/contact-form-7/wp-contact-form-7.php';
            if ( file_exists($plugin_file) ) {
                $plugin_data = get_plugin_data($plugin_file);
                if ( version_compare($plugin_data['Version'], '5.0') < 0 ) {
                    $this->add_admin_notice('ERROR: The Compulse Encrypted Fields plugin requires Contact Form 7 version 5.0 or higher.', 'error');
                }
            }
        }
    }

    public function add_options_page() {
        register_setting('cim-encrypted-fields-options', 'cim_form_encryption_public_key');
        register_setting('cim-encrypted-fields-options', 'cim_form_encryption_use_aes_encryption');
        register_setting('cim-encrypted-fields-options', 'cim_form_encryption_disable_cf7');
        register_setting('cim-encrypted-fields-options', 'cim_form_encryption_disable_wp_job_manager');

        add_settings_section('cim-encrypted-fields-options-main-section', 'Compulse Encrypted Fields Options', '__return_empty_string', 'cim-encrypted-fields-options');

        add_settings_field(
            'cim_form_encryption_disable_cf7',
            'Disable Contact Form 7 Field',
            array($this, 'checkbox_option'),
            'cim-encrypted-fields-options',
            'cim-encrypted-fields-options-main-section',
            array(
                'name' => 'cim_form_encryption_disable_cf7',
                'description' => 'Check this box to disable the Contact Form 7 encrypted field.',
                'checked' => !!get_option('cim_form_encryption_disable_cf7', false),
                'label' => 'Disable'
            )
        );

        add_settings_field(
            'cim_form_encryption_disable_wp_job_manager',
            'Disable WP Job Manager Applications Field',
            array($this, 'checkbox_option'),
            'cim-encrypted-fields-options',
            'cim-encrypted-fields-options-main-section',
            array(
                'name' => 'cim_form_encryption_disable_wp_job_manager',
                'description' => 'Check this box to disable the WP Job Manager Applications encrypted field.',
                'checked' => !!get_option('cim_form_encryption_disable_wp_job_manager', false),
                'label' => 'Disable'
            )
        );

        add_settings_field(
            'cim_form_encryption_use_aes_encryption',
            'Use AES Encryption',
            array($this, 'checkbox_option'),
            'cim-encrypted-fields-options',
            'cim-encrypted-fields-options-main-section',
            array(
                'name' => 'cim_form_encryption_use_aes_encryption',
                'description' => 'Check this box to use AES encryption instead of PGP encryption.  If this is enabled, emails will not be encrypted.',
                'checked' => !!get_option('cim_form_encryption_use_aes_encryption', false),
                'label' => 'Yes'
            )
        );

        add_settings_field(
            'cim_form_encryption_public_key',
            'PGP Public Key',
            array($this, 'textarea_option'),
            'cim-encrypted-fields-options',
            'cim-encrypted-fields-options-main-section',
            array(
                'name' => 'cim_form_encryption_public_key',
                'description' => 'The public key used to encrypt.',
                'value' => get_option('cim_form_encryption_public_key', '')
            )
        );

        add_options_page('Compulse Encrypted Fields', 'Compulse Encrypted Fields', 'manage_options', 'cim-encrypted-fields-options', array($this, 'options_page_content'));
    }

    public function checkbox_option($args = array()) {
        $args = array_merge([
            'name' => '',
            'description' => '',
            'label' => '',
            'checked' => false
        ], $args);

        printf(
            '<label><input type="checkbox" name="%s"%s /> %s</label><div>%s</div>',
            $args['name'],
            $args['checked'] ? ' checked' : '',
            $args['label'],
            $args['description']
        );
    }

    public function textarea_option($args = array()) {
        $args = array_merge([
            'name' => '',
            'description' => '',
            'value' => ''
        ], $args);

        printf('<textarea name="%s" style="width:100%%; height:200px;">%s</textarea><div>%s</div>', $args['name'], $args['value'], $args['description']);
    }

    public function options_page_content() {
        include CIM_FORM_ENCRYPTION_PLUGIN_DIR . '/templates/options-page.php';
    }

    public function add_admin_notice( $message, $type = 'success' ) {
        $this->admin_notices[] = array(
            'message' => $message,
            'type' => $type
        );
    }

    public function admin_notices() {
        foreach ( $this->admin_notices as $notice ): ?>
            <div class="notice notice-<?php print $notice['type']; ?> is-dismissible">
                <p><?php print $notice['message']; ?></p>
            </div>
        <?php endforeach;
    }
}
