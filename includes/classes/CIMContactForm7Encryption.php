<?php

error_log(print_r($_POST, true));

class CIMContactForm7Encryption {
    private static $instance = null;

    public static function get_instance() {
        if ( empty(self::$instance) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    // holds original encrypted values submitted through the contact form.
    private $encrypted_values;

    // holds the outgoing message after it has been encrypted.
    private $encrypted_message;

    // Contact Form 7 DB option name for if the submission time should be added to the posted data.
    private $cf7db_generate_submit_time_option;

    // Contact Form 7 DB submits table name in the database.
    private $cf7db_submits_table_name;

    // The Contact Form 7 DB submit time that is currently being processed.
    private $cf7db_last_submit_time;

    // The Contact Form 7 DB settings page hook name.
    private $cf7db_submission_page_hook;


    private function __construct() {
        global $wpdb;

        $this->encrypted_values = array();
        $this->encrypted_message = "";
        $this->cf7db_generate_submit_time_option = "CF7DBPlugin_GenerateSubmitTimeInCF7Email";
        $this->cf7db_submits_table_name = $wpdb->prefix . "cf7dbplugin_submits";
        $this->cf7db_last_submit_time = false;
        $this->cf7db_submission_page_hook = 'toplevel_page_CF7DBPluginSubmissions';

        add_action('wpcf7_init', array($this, 'init'));
        add_action('wpcf7_admin_init', array($this, 'add_tag_generator'), 50);
        add_action('wpcf7_before_send_mail', array($this, 'before_send_mail'));
        add_filter('wpcf7_posted_data', array($this, 'posted_data'), 50); // Contact Form DB submission time is hooked at 10.

        add_action( $this->cf7db_submission_page_hook, array($this, 'show_resend_message_form'), 50 );
        add_action('wp_ajax_cim_resend_encrypted_message', array($this, 'handle_resend_message_ajax'));

        // Turn on the option to generate the submit time in the submission email, so that data is accessible.
        if ( get_option($this->cf7db_generate_submit_time_option, 'false') != 'true' ) {
            update_option($this->cf7db_generate_submit_time_option, 'true');
        }
    }

    public function init() {
        // Add the [encrypted ...] tag.
        if ( function_exists('wpcf7_add_form_tag') ) {
            wpcf7_add_form_tag(array('encrypted', 'encrypted*'), array($this, 'encrypted_shortcode'), true);
        } else if (function_exists('wpcf7_add_shortcode')) {
            wpcf7_add_shortcode('encrypted', array($this, 'encrypted_shortcode'), true);
            //wpcf7_add_shortcode('encrypted*', array($this, 'encrypted_shortcode'), true);
        }

        // Validation filters.
        add_filter( 'wpcf7_validate_encrypted', array($this, 'validate'), 10, 2 );
        add_filter( 'wpcf7_validate_encrypted*', array($this, 'validate'), 10, 2 );
    }

    public function validate( $result, $tag ) {
        $name = $tag->name;

    	$value = isset( $_POST[$name] )
    		? trim( wp_unslash( strtr( (string) $_POST[$name], "\n", " " ) ) )
    		: '';

		if ( $tag->is_required() && '' == $value ) {
			$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
		}

        return $result;
    }

    public function preg_replace_decrypt_callback($matches) {
        $value = cim_form_encryption()->decrypt_aes_message( $matches[1] );

        return $value;
    }

    public function show_resend_message_form() {
        global $wpdb;

        $submit_time = filter_input(INPUT_GET, 'submit_time', FILTER_VALIDATE_REGEXP, [
            'options' => [
                'regexp' => '/^[0-9]+\.[0-9]+$/'
            ]
        ]);

        if ( !empty( $submit_time ) && current_user_can( 'administrator' ) ) {
            if ( cim_form_encryption()->get_encryption_method() == 'AES' ) {
                $query = "SELECT `field_value` FROM `" . $this->cf7db_submits_table_name . "` WHERE `submit_time` = %s AND `field_name` = %s";
                $encrypted_message = $wpdb->get_var( $wpdb->prepare($query, $submit_time, 'encrypted_message') );
                if ( !empty( $encrypted_message ) ) {
                    print '<h3>Decrypted message</h3>';
                    print '<pre>';
                    $decrypted_message = cim_form_encryption()->decrypt_aes_message( $encrypted_message );
                    // Decrypt the encrypted fields in this message.
                    $decrypted_message = preg_replace_callback('/\[\[cim\-encrypted\$([^\]]+)\]\]/', array($this, 'preg_replace_decrypt_callback'), $decrypted_message);
                    print $decrypted_message;
                    print '</pre>';
                }
            } else {
                include CIM_FORM_ENCRYPTION_PLUGIN_DIR . '/templates/cf7db-resend-message-form.php';
            }
        }
    }

    public function handle_resend_message_ajax() {
        global $wpdb;

        if ( !current_user_can('administrator') )
            exit;

        $post_filter = filter_input_array(INPUT_POST, [
            'to' => FILTER_DEFAULT,
            'submitTime' => FILTER_DEFAULT
        ]);

        if ( !empty($post_filter['to']) && !empty($post_filter['submitTime']) ) {
            $to = $post_filter['to'];
            $submit_time = $post_filter['submitTime'];

            $query = "SELECT `field_value` FROM `" . $this->cf7db_submits_table_name . "` WHERE `submit_time` = %s AND `field_name` = %s";
            $encrypted_message = $wpdb->get_var( $wpdb->prepare($query, $submit_time, 'encrypted_message') );

            if ( !empty($encrypted_message) ) {
                // unhook encryption, so it's not encrypted again.
                remove_filter('wp_mail', array('WP_PGP_Encrypted_Emails', 'wp_mail'));
                wp_mail($to, 'Encrypted Contact Form Submission', $encrypted_message);
                wp_send_json_success();
            } else {
                wp_send_json([
                    'error' => 'Encrypted message is empty.'
                ]);
            }
        }

        exit;
    }

    public function before_send_mail($cf7) {
        // Happens before contact form email is sent.
        add_action('wp_mail', array($this, 'encrypt_and_store_message'), 50);
    }

    /**
     * Called before the message is sent.
     */
    public function encrypt_and_store_message($atts) {
        global $wp_filter, $wpdb;

        // Remove this action, so subsequent calls to wp_mail don't trigger it.
        remove_action('wp_mail', array($this, 'encrypt_and_store_message'), 20);

        // Check if currently submitted form has any encrypted values.  If not, encryption is not necessary.
        if ( !empty($this->encrypted_values) ) {
            // Try to encrypt the message.
            $encryption_result = cim_form_encryption()->encrypt_message($atts['message']);
            if ( $encryption_result ) {
                $this->encrypted_message = $encryption_result;

                if ( cim_form_encryption()->get_encryption_method() == 'PGP' ) {
                    // Encrypt the email message.
                    $atts['message'] = $encryption_result;
                }

                // Store the encrypted message in Contact Form DB.
                // Get the maximum field_order value.
                $max_query = "SELECT MAX(`field_order`) FROM `" . $this->cf7db_submits_table_name . "` WHERE `submit_time` = %s";
                $max_field_order = $wpdb->get_var( $wpdb->prepare($max_query, $this->cf7db_last_submit_time) );

                $query = "INSERT INTO `" . $this->cf7db_submits_table_name . "` (`submit_time`, `form_name`, `field_name`, `field_value`, `field_order`) VALUES (%s, %s, %s, %s, %s)";

                if ( $this->cf7db_last_submit_time > 0 ) {
                    $wpdb->query( $wpdb->prepare($query, $this->cf7db_last_submit_time, 'Contact Form', 'encrypted_message', $this->encrypted_message, $max_field_order + 1) );
                }
            }
        }

        return $atts;
    }

    public function posted_data($data) {
        $contact_form_id = $data['_wpcf7'];

        if ( array_key_exists('submit_time', $data) ) {
            $this->cf7db_last_submit_time = $data['submit_time'];
        }

        // get contact form
        // scan for encrypted fields.

        $submission = WPCF7_Submission::get_instance();
        $scanned_tags = $submission->get_contact_form()->scan_form_tags();

        // Store and erase encrypted values.
        foreach ( $scanned_tags as $tag ) {
            if ( $tag->type == 'encrypted' || $tag->type == 'encrypted*' ) {
                $tag_name = $tag->name;

                if ( array_key_exists($tag_name, $data) ) {
                    $this->encrypted_values[ $tag_name ] = $data[ $tag_name ];
                    if ( cim_form_encryption()->get_encryption_method() == 'AES' ) {
                        $data[ $tag_name ] = '[[cim-encrypted$' . cim_form_encryption()->encrypt_message( $data[ $tag_name ], 'AES' ) . ']]';
                    } else {
                        $data[ $tag_name ] = '*****';
                    }
                }
            }
        }
        /*
        <label> SSN
    [encrypted* ssn id:test class:testc]</label>
        */

        return $data;
    }

    /**
     * Handle the CF7 shortcode [encrypted ...]
     */
    public function encrypted_shortcode( $tag ) {
        $tag = new WPCF7_FormTag($tag);

        if ( empty( $tag->name ) ) {
    		return '';
    	}

    	$validation_error = wpcf7_get_validation_error( $tag->name );

    	$class = wpcf7_form_controls_class( $tag->type, 'wpcf7-text' );

    	if ( $validation_error ) {
    		$class .= ' wpcf7-not-valid';
    	}

    	$atts = array();

    	$atts['size'] = $tag->get_size_option( '40' );
    	$atts['maxlength'] = $tag->get_maxlength_option();
    	$atts['minlength'] = $tag->get_minlength_option();

    	if ( $atts['maxlength'] && $atts['minlength']
    	&& $atts['maxlength'] < $atts['minlength'] ) {
    		unset( $atts['maxlength'], $atts['minlength'] );
    	}

    	$atts['class'] = $tag->get_class_option( $class );
    	$atts['id'] = $tag->get_id_option();
    	$atts['tabindex'] = $tag->get_option( 'tabindex', 'int', true );

    	$atts['autocomplete'] = $tag->get_option( 'autocomplete',
    		'[-0-9a-zA-Z]+', true );

    	if ( $tag->has_option( 'readonly' ) ) {
    		$atts['readonly'] = 'readonly';
    	}

    	if ( $tag->is_required() ) {
    		$atts['aria-required'] = 'true';
    	}

    	$atts['aria-invalid'] = $validation_error ? 'true' : 'false';

    	$value = (string) reset( $tag->values );

    	if ( $tag->has_option( 'placeholder' ) || $tag->has_option( 'watermark' ) ) {
    		$atts['placeholder'] = $value;
    		$value = '';
    	}

    	$value = $tag->get_default_option( $value );

    	$value = wpcf7_get_hangover( $tag->name, $value );

    	$atts['value'] = $value;

        $atts['type'] = 'text';

    	$atts['name'] = $tag->name;

    	$atts = wpcf7_format_atts( $atts );

    	$html = sprintf(
    		'<span class="wpcf7-form-control-wrap %1$s"><input %2$s />%3$s</span>',
    		sanitize_html_class( $tag->name ), $atts, $validation_error );

    	return $html;
    }

    /**
     * Add the generator pane for the [encrypted ...] shortcode in the backend.
     */
    public function add_tag_generator() {
        $tag_generator = WPCF7_TagGenerator::get_instance();
        $tag_generator->add( 'encrypted', 'encrypted', array($this, 'tag_generator_pane'), array(
            // any options go here.
        ) );
    }

    /**
     * Display the generator pane.
     */
    public function tag_generator_pane($args = array()) {
        include CIM_FORM_ENCRYPTION_PLUGIN_DIR . '/templates/cf7-tag-generator-pane.php';
    }

    public function mail_tag_replaced($replaced, $submitted, $html, $mail_tag) {
        // var_dump($replaced, $submitted, $html, $mail_tag);
        return $replaced;
    }
}
