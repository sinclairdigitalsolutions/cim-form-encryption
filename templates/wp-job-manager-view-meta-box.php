<?php

$ajax_url = admin_url('admin-ajax.php');

?>

<div class="view-encrypted-message" data-id="<?php print get_the_ID(); ?>" data-ajax-url="<?php print $ajax_url; ?>">
    <div>Click to view encrypted fields.</div>
    <div style="margin:10px 0;">
        <input type="button" value="View" class="button" />
    </div>
</div>

<div class="encrypted-message-content">
	
</div>

<script>
    (function($) {
        var container = $('.view-encrypted-message');
        var button = container.find('.button');
        var processing = false;

        button.on('click', function(e) {
            if ( processing )
                return;

            var ajaxUrl = container.data('ajax-url');
            var id = container.data('id');

			processing = true;
			button.prop('disabled', true).val('Decrypting...');
			$.post(ajaxUrl, {
				'action': 'cim_view_encrypted_job_application',
				'id': id
			}, function(data) {
				console.log(data);

				processing = false;
				button.prop('disabled', false).val('View');

				if ( data.html ) {
					$('.encrypted-message-content').html(data.html);
				}
			});

            e.preventDefault();
            return false;
        });
    })(jQuery);
</script>
