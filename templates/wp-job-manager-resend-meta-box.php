<?php

$ajax_url = admin_url('admin-ajax.php');

?>

<div class="resend-encrypted-message" data-id="<?php print get_the_ID(); ?>" data-ajax-url="<?php print $ajax_url; ?>">
    <div>Use this form to resend the encrypted notification for this job application.</div>
    <div style="margin:10px 0;">
        <label>
            <input type="text" placeholder="Email Address" style="display:block; width:100%;" class="resend-to" />
        </label>
    </div>
    <div style="margin:10px 0;">
        <input type="button" value="Send" class="button" />
    </div>
</div>

<script>
    (function($) {
        var container = $('.resend-encrypted-message');
        var button = container.find('.button');
        var processing = false;

        button.on('click', function(e) {
            if ( processing )
                return;

            var ajaxUrl = container.data('ajax-url');
            var id = container.data('id');
            var to = container.find('input.resend-to').val();

            if ( to ) {
                processing = true;
                button.prop('disabled', true).val('Sending...');
                $.post(ajaxUrl, {
                    'action': 'cim_resend_encrypted_job_application',
                    'to': container.find('input.resend-to').val(),
                    'id': id
                }, function(data) {
                    console.log(data);

                    processing = false;
                    button.prop('disabled', false).val('Send');

                    if ( data.success ) {
                        var successDiv = $('<div>Successfully sent.</div>');
                        button.after(successDiv);
                        setTimeout(function() {
                            successDiv.remove();
                        }, 10000)
                    }
                });
            }

            e.preventDefault();
            return false;
        });
    })(jQuery);
</script>
