<?php

$submit_time = "";
if ( isset($_GET['submit_time']) ) {
    $submit_time = preg_replace('/[^0-9\.]/', '', $_GET['submit_time']);
}

$ajax_url = admin_url('admin-ajax.php');

?>

<div class="wrap">
    <h3>Resend Encrypted Message</h3>
    <form action="#" id="resend-encrypted-message" data-ajax-url="<?php print esc_attr($ajax_url); ?>" data-submit-time="<?php print esc_attr($submit_time); ?>">
        <label>
            Send To: <input type="email" name="email" value="" />
        </label>
        <input type="submit" value="Resend" class="button" />
    </form>
    <div class="resend-encrypted-message-response"></div>
</div>


<script>
    (function($) {
        $('form#resend-encrypted-message').submit(function(e) {
            var form = $(this);
            var ajaxUrl = form.data('ajax-url');
            var submitTime = form.data('submit-time');
            var email = form.find("input[name='email']").val();

            var response = $(".resend-encrypted-message-response");

            if ( email && submitTime ) {
                var ajaxData = {
                    'action': 'cim_resend_encrypted_message',
                    'to': email,
                    'submitTime': submitTime
                };

                response.html('Sending...');

                $.post(ajaxUrl, ajaxData, function(data) {
                    console.log(data);
                    if (data.success) {
                        response.html('Successfully sent.');
                    } else {
                        response.html('Failed to send.');
                    }
                });
            }

            e.preventDefault();
            return false;
        });
    })(jQuery);
</script>
